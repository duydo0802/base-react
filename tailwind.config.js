/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        bghover: "rgba(16, 125, 159, 0.1)",
        bg1: "rgba(0, 0, 0, 0.8)",
        bd1: "rgba(16, 125, 159, 1)",
        green: "rgba(74, 166, 79, 1)",
        yellow: "rgba(236, 202, 44, 1)",
        "yellow-secondary": "#F59E0B",
        orange: "rgba(241, 89, 42, 1)",
        brown: "rgba(120,64,50,1)",
        "brown-secondary": "rgba(245, 158, 11, 1)",
        "grey-600": "#111111",
        "grey-500": "#69696B",
        "grey-400": "#ABABB5",
        "grey-300": "#D5D5DE",
        "grey-200": "#EBEBF0",
        "grey-100": "#FFFFFF",
        red: "#E93700",
        blue: "#107D9F",
        primary: "#107D9F",
      },
    },
    fontFamily: {
      inter: ['Inter', 'san-serif'],
      notoJP: ['Noto Sans JP', 'san-serif'],
      roboto: ['Roboto', 'san-serif'],
      manrope: ['Manrope', 'san-serif'],
    }
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  },
}
