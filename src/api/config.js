import { notification } from 'antd';
import axios from 'axios';

import requestTable from '../utils/constants/requestTable';

const API_URL = process.env.REACT_APP_API_URL;

const instance = axios.create({
  baseURL: API_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Cache-Control': 'no-cache, no-store, must-revalidate',
  },
});

export const setTokenAuthen = (token) => {
  if (token) {
    localStorage.setItem('token', token);
    instance.defaults.headers.common.Authorization = `Bearer ${token}`;
  } else {
    delete instance.defaults.headers.common.Authorization;
    localStorage.removeItem('token');
  }
};

(() => {
  const token = window.localStorage.getItem('token');
  if (token) {
    setTokenAuthen(token);
  }
})();

export const getUrl = (apiName, ids) => {
  let urlString = requestTable[apiName];
  if (ids && ids.length > 0) {
    for (let i = 0; i < ids.length; i += 1) {
      urlString = urlString.replace(':id', `${ids[i]}`);
    }
  }
  return urlString;
};

export const fetcher = async ({
  method,
  url,
  params,
  data,
  cancelToken,
  responseType,
}) => {
  try {
    const { data: res } = await instance.request({
      method,
      url,
      params,
      data,
      cancelToken,
      responseType,
    });
    if (res?.code) {
      throw Error(res?.message);
    }
    return res;
  } catch (e) {
    if (axios.isCancel(e)) {
      console.log('Cancel request');
      return null;
    }
    if (axios.isAxiosError(e)) {
      const errorCode = Number(e.response?.status);
      if (errorCode === 401) {
        setTokenAuthen('');
        window.location.href = '/sign-in';
        return null;
      }
      if (errorCode === 404) {
        window.location.href = '/page-not-found';
        return null;
      }
      if (!window.navigator.onLine) {
        window.location.reload();
        return null;
      }
      throw e;
    } else {
      notification.error({
        message: e.message,
        className: 'notification-error',
      });
      throw e;
    }
  }
};
