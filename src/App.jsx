import { Route, Switch } from 'react-router-dom';

import PrivateRoute from './components/common/PrivateRoute';
import AuthLayout from './components/layout/AuthLayout';
import MainLayout from './components/layout/MainLayout';
import PATH_NAME from './utils/constants/pathName';

import SignIn from './pages/sign-in';

import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path={PATH_NAME.SIGN_IN}>
          <AuthLayout>
            <SignIn />
          </AuthLayout>
        </Route>
        <PrivateRoute exact path={PATH_NAME.HOME}>
          <MainLayout>
            <div>Main</div>
          </MainLayout>
        </PrivateRoute>
      </Switch>
    </div>
  );
}

export default App;
