const MainLayout = ({ children }) => {
  return (
    <div className="h-full w-full">
      <div>This is main layout</div>
      <div>{children}</div>
    </div>
  )
}

export default MainLayout;
