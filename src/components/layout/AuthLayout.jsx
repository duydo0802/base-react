const AuthLayout = ({ children }) => {
  return (
    <div className="h-full w-full bg-red">
      <div className="text-lg">This is authentication layout.</div>
      <div>{children}</div>
    </div>
  )
};

export default AuthLayout;
