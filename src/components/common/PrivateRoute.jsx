import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Route, useHistory } from 'react-router-dom'
import { isAuth } from '../../store/auth';
import PATH_NAME from '../../utils/constants/pathName';


const PrivateRoute = (route) => {
  const navigator = useHistory();
  const isLogin = useSelector(isAuth);

  useEffect(() => {
    if (!isLogin) {
      navigator.push(`${PATH_NAME.LOGIN}`);
    }
  }, [isLogin, navigator]);

  return <Route {...route} />;
}

export default PrivateRoute;
