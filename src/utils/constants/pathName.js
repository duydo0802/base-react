const PATH_NAME = {
  HOME: '/',
  SIGN_IN: '/sign-in',
  REGISTER: '/register',
};

export default PATH_NAME;
