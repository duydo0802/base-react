import { createSlice } from '@reduxjs/toolkit';
import { setTokenAuthen } from '../../api/config';

const AUTH_INIT_STATE = {
  isAuth: !!localStorage.getItem('token'),
  token: localStorage.getItem('token') || '',
};

export const authSlice = createSlice({
  name: 'auth',
  initialState: AUTH_INIT_STATE,
  reducers: {
    setIsAuth: (state, action) => {
      state.isAuth = action.payload;
    },
    setToken: (state, action) => {
      state.token = action.payload;
      setTokenAuthen(action.payload);
    },
    logout: (state) => {
      state.token = '';
      setTokenAuthen('');
      state.isAuth = false;
      window.localStorage.removeItem('token');
    },
  },
});

export const { setIsAuth, setToken, logout, setUserDetail } = authSlice.actions;

export const isAuth = (state) => state.authSlice.isAuth;

export default authSlice.reducer;
