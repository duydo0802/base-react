## Cách chạy project

### Tải các dependencies

`npm install`

### Khởi chạy dự án

`npm run start`

Truy cập vào đường dẫn http://localhost:3000

## Cấu trúc thư mục src của dự án

#### Thư mục api (Đây là thư mục cấu hình và định nghĩa API)

#### Thư mục assets (Đây là thư mục chứa ảnh của dự án)

#### Thư mục components (Đây là thư mục chứa các components)
1. Thư mục common định nghĩa các component dùng chung.
2. Thư mục layout định nghĩa các layout màn hình.

#### Thư mục pages (Đây là thư mục định nghĩa các trang tương ứng với từng route của dự án)

#### Thư mục store (Đây là thư mục tập chung quản lý các state dùng chung trong toàn bộ dự án)

#### Thư mục utils (Đây là thư mục hỗ trợ định nghĩa các biến, các hàm dùng chung cho toàn bộ dự án)

1. Thư mục constants định nghĩa các biến hay các hằng số dùng chung ở toàn bộ dự án.
2. Thư mục helpers định nghĩa các hàm hỗ trợ trong dự án.
3. Thư mục hook định nghĩa các hook dùng chung trong dự án.